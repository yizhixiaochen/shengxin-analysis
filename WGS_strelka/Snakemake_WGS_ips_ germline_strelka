from glob import iglob
import pandas as pd

r1 = iglob('/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/ips/*_L1_1.fq.gz')

samples = pd.DataFrame()
samples['r1'] = [i for i in r1]
samples['cell'] = samples.r1.str.extract('/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/ips/(.*)_L1_1.fq.gz', expand=True)

samples2 = samples.loc[~samples.cell.isin(["ips3-3-1-LED4579"])]

rule all:
	input:
		#list('fastqc_output/' + samples['cell'] + "_R1_fastqc.html"),
		#list('fastqc_output/' + samples['cell'] + "_R2_fastqc.html"),
		list('Trim/' + samples['cell'] + '_R1_val_1.fq.gz'),
		list('Trim/' + samples['cell'] + '_R2_val_2.fq.gz'),
		list('Mapping/' + samples['cell'] + '.sorted.markdup.bam'),
		list('Mapping/' + samples['cell'] + '.sorted.markdup.bam.bai'),
		"Strelka_merge/ips3-3-1-LED4579/results/variants/genome.vcf.gz",
		"Strelka_merge/ips5-1-LED4578/results/variants/genome.vcf.gz",
		list('Split_Variants/' + samples['cell'] + '.snp.vcf.gz'),
		list('Split_Variants/' + samples['cell'] + '.indel.vcf.gz'),
		list('PASS_Variants/' + samples['cell'] + '.snp.vcf'),
		list('PASS_Variants/' + samples['cell'] + '.indel.vcf'),
		list('Annovar_all/' + samples['cell'] + '.snp.hg38_multianno.txt'),
		list('Annovar_all/' + samples['cell'] + '.indel.hg38_multianno.txt'),
		list('Final_results/' + samples['cell'] + '.snp.txt'),
		list('Final_results/' + samples['cell'] + '.indel.txt'),

#rule fastqc:
#	input:
#		r1 = '/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/Zebrafish/{cell}_L0_1.fq.gz',
#		r2 = '/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/Zebrafish/{cell}_L0_2.fq.gz'
#	output:
#		R1 = 'fastqc_output/{cell}_R1_fastqc.html',
#		R2 = 'fastqc_output/{cell}_R2_fastqc.html'
#	shell:
#		'''
#			fastqc -o fastqc_output -t 20 {input.r1} {input.r2}
#
#		'''
rule Trim:
	input:
		R1 = '/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/ips/{cell}_L1_1.fq.gz',
		R2 = '/dssg/home/sw_zxq/xiaochen/Raw_data/JinYinGe/WGS/ips/{cell}_L1_2.fq.gz'
	output:
		R1 = 'Trim/{cell}_R1_val_1.fq.gz',
		R2 = 'Trim/{cell}_R2_val_2.fq.gz'
	shell:
		'''
		fastp \
		-w 40 -l 25 \
		--detect_adapter_for_pe \
		-i {input.R1} -I {input.R2} \
		-o {output.R1} -O {output.R2}
		'''
rule bwa:
	input:
		R1 = 'Trim/{cell}_R1_val_1.fq.gz',
		R2 = 'Trim/{cell}_R2_val_2.fq.gz'
	output:
		"Mapping/{cell}.sorted.bam"
	shell:
		'''
		bwa mem -t 40 -M \
		/dssg/home/sw_zxq/xiaochen/Reference/human/UCSC/hg38.chrom.fasta \
		{input.R1} {input.R2} | \
		samtools view -@ 40 -Sb | \
		samtools sort -@ 40 -T Mapping/{wildcards.cell}.tmp -o {output}
		'''
rule markdump:
	input:
		"Mapping/{cell}.sorted.bam"
	output:
		temp("Mapping/{cell}.sorted.markdup.bam")
	shell:
		'''
		sambamba markdup -t 40 -r -p {input} {output}
		'''
rule index:
	input:
		"Mapping/{cell}.sorted.markdup.bam"
	output:
		temp("Mapping/{cell}.sorted.markdup.bam.bai")
	shell:
		'samtools index {input}'
rule strelka_1:
	input:
		bam = 'Mapping/ips3-3-1-LED4579.sorted.markdup.bam',
		ref = '/dssg/home/sw_zxq/xiaochen/Reference/human/UCSC/hg38.chrom.fasta',
		bai = 'Mapping/ips3-3-1-LED4579.sorted.markdup.bam.bai'
	output:
		"Strelka_merge/ips3-3-1-LED4579/results/variants/genome.vcf.gz"
	shell:
		'''
		configureStrelkaGermlineWorkflow.py \
		--bam {input.bam} \
		--referenceFasta {input.ref} \
		--runDir Strelka_merge/ips3-3-1-LED4579
		
		Strelka_merge/ips3-3-1-LED4579/runWorkflow.py -m local -j 20
		'''
rule strelka_2:
	input:
		bam = 'Mapping/ips5-1-LED4578.sorted.markdup.bam',
		ref = '/dssg/home/sw_zxq/xiaochen/Reference/human/UCSC/hg38.chrom.fasta',
		bai = 'Mapping/ips5-1-LED4578.sorted.markdup.bam.bai'
	output:
		"Strelka_merge/ips5-1-LED4578/results/variants/genome.vcf.gz"
	shell:
		'''
		configureStrelkaGermlineWorkflow.py \
		--bam {input.bam} \
		--referenceFasta {input.ref} \
	 	--runDir Strelka_merge/ips5-1-LED4578
		
		Strelka_merge/ips5-1-LED4578/runWorkflow.py -m local -j 20
		'''
rule SelectVariants:
	input:
		"Strelka_merge/{cell}/results/variants/genome.vcf.gz"
	output:
		snp = 'Split_Variants/{cell}.snp.vcf.gz',
		indel = 'Split_Variants/{cell}.indel.vcf.gz'
	shell:
		'''
		gatk SelectVariants -R /dssg/home/sw_zxq/xiaochen/Reference/human/UCSC/hg38.chrom.fasta \
			--discordance /dssg/home/sw_zxq/xiaochen/Reference/human/gatk/dbsnp_146.hg38.vcf.gz -select-type SNP --restrict-alleles-to BIALLELIC --variant {input} -O {output.snp}
		gatk SelectVariants -R /dssg/home/sw_zxq/xiaochen/Reference/human/UCSC/hg38.chrom.fasta \
			--discordance /dssg/home/sw_zxq/xiaochen/Reference/human/gatk/dbsnp_146.hg38.vcf.gz -select-type INDEL --restrict-alleles-to BIALLELIC --variant {input} -O {output.indel}
		'''

rule add_col3:
	input:
		snp = 'Split_Variants/{cell}.snp.vcf.gz',
		indel = 'Split_Variants/{cell}.indel.vcf.gz'
	output:
		snp = 'PASS_Variants/{cell}.snp.vcf',   
		indel = 'PASS_Variants/{cell}.indel.vcf'
	shell:
		'''
		zgrep -v '#' {input.snp} | awk '$3=$2'| sed 's/ /'$'\t/g' > {output.snp}
		zgrep -v '#' {input.indel} | awk '$3=$2+length($4)-1'| sed 's/ /'$'\t/g' > {output.indel}
		'''
rule annovar_all:
	input:
		snp = 'PASS_Variants/{cell}.snp.vcf',
		indel = 'PASS_Variants/{cell}.indel.vcf'
	output:
		snp = "Annovar_all/{cell}.snp.hg38_multianno.txt",
		indel = "Annovar_all/{cell}.indel.hg38_multianno.txt"
	shell:
		'''
		/dssg/home/sw_zxq/xiaochen/BioSoft/annovar/table_annovar.pl {input.snp} ~/xiaochen/BioSoft/annovar/humandb/ -buildver hg38 -out Annovar_all/{wildcards.cell}.snp -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
		/dssg/home/sw_zxq/xiaochen/BioSoft/annovar/table_annovar.pl {input.indel} ~/xiaochen/BioSoft/annovar/humandb/ -buildver hg38 -out Annovar_all/{wildcards.cell}.indel -remove -protocol refGene,avsnp150 -operation gx,f -nastring .
		'''
rule clean:
	input:
		snp = "Annovar_all/{cell}.snp.hg38_multianno.txt",
		indel = "Annovar_all/{cell}.indel.hg38_multianno.txt"
	output:
		snp = "Final_results/{cell}.snp.txt",
		indel = "Final_results/{cell}.indel.txt"
	shell:
		'''
		awk -F '\t' '{{print$1,$2,$3,$4,$5,$6,$7,$11}}' {input.snp} | grep -v rs[0-9] | awk '$9=$1 "_" $2 "_" $4 "_" $5' | sed 's/ /'$'\t/g' > Final_results/{wildcards.cell}.snp.txt
		awk -F '\t' '{{print$1,$2,$3,$4,$5,$6,$7,$11}}' {input.indel} | grep -v rs[0-9] | awk '$9=$1 "_" $2 "_" $4 "_" $5' | sed 's/ /'$'\t/g' > Final_results/{wildcards.cell}.indel.txt
		'''
